\chapter{Jälleenkuvaukset}
\label{cha:jälleenkuvaukset}

\citet{galbrun18redescription} esittelee teoksessaan samat peruskäsitteet joita tässä käsitellään ja kappale pohjautuukin siihen. 
Tässä kappaleessa on tarkoituksena käydä läpi kaikki peruskäsitteet joista moni rakentuu hierarkisesti toistensa päälle. 
Lopulta pääsemme \textit{jälleenkuvaksiin} ja niiden määritelmiin.

\section{Peruskäsitteet}
Tässä osiossa käsitellään peruskäsitteitä, joita tarvitaan kun käsittelemme ylemmän tason 
käsitteitä. Peruskäsitteitä ovat \textit{entiteetti (entity)}, \textit{attribuutti (attribute)}, \textit{näkymä (view)}, \textit{data (data)}, \textit{taulu (table)}, 
\textit{perdikaatti (predicate)}, \textit{literaali (literal)} ja \textit{kysely (query)}

\subsection{Entiteetit, attribuutit ja näkymät}
Yksinkertaisimpia käsitteitä joita jälleenkuvauksissa käsitellään ovat entiteettien joukko 
sekä niille assosioitu attribuuttien joukko. Jokainen entiteetti $e \in \mathcal{E}$ sekä 
jokainen attribuutti $a \in \mathcal{A}$. Tietyn entiteetin attribuutin arvoa merkitään $a(e)$, 
mutta yksinkertaistamisen takia käytetään merkintää $a$ sekä itse attribuutista, että sen arvosta. 
Attribuutin tyyppi voi olla joko totuusarvo, numeerinen arvo tai kategorinen arvo.

\indent Näkymien joukko $\mathcal{V}$ on $\mathcal{A}$:n osajoukko, jossa yksittäisille näkymille $V$ pätee 
$V_i \cap V_j = \emptyset$, jossa $i \neq j$. Näkymä attribuutille on merkitään $view(a)$. 
Näkymiä tulee olla vähintään kaksi. Mikäli attribuuteille ei löydy selkeää jakajaa, 
voidaan jakaa attribuutit yksittäisiin näkymiin, jolloin näkymiä on $|\mathcal{A}|$ kappaletta.

\subsection{Data ja taulut}
Data koostuu taulusta $\mathcal{D} = (\mathcal{E}, \mathcal{A}, \mathcal{V})$. Eli data koostuu kaikista entiteeteistä, 
niiden attribuuteista ja näkymistä jotka jakavat attribuutit osiin. Data esitetään taulumallina, 
jossa on vähintään yksi taulu $D_n$. Taulu koostuu riveistä, jotka ovat entiteettejä, kolumnit attribuutteja ja 
solun arvo $D_k (i, j)$, jossa $i$ on entiteetti ja $j$ attribuutti taulussa $k$. Mikäli tauluja on kaksi tai useampi, 
tulee jokaisen rivin entiteetin vastata toisen taulun riviä ja siten myös entiteettiä.

\subsection{Predikaatit ja literaalit}
Perdikaatti jollekin attribuutille $a$ on funktio $p_a:\mathcal{E}\rightarrow \{true, false\}$. 
Predikaatti palauttaa siis totuusarvon atrribuutin arvosta riippuen. Joukko kaikista predikaateista 
ja niiden negaatioista on literaalien joukko $\mathcal{L}$.
Riippuen kuitenkin attribuutin tyypistä, predikaatti voi tarvita lisäsääntöjä totuusarvon määrittämiseksi.
Jos attribuutin arvo on totuusarvoinen, niin funktio palauttaa sen. 
Mikäli on kuitenkin kyse numeerisesta tai kategorisesta 
arvosta, tarvitaan \textit{looginen ehdotus(logical proposition)}, joka arvon perusteella palauttaa totuusarvon säännön mukaan.
Loogista ehdotusta merkitään $P_a(e)$, attribuutille $a$ ja entiteetille $e$.
Tästä seuraa siis, että $P_a(e) = p_a(e)$. 

Esimerkkinä numeerisen arvon ehdolle voisi olla $[0 \leq a \leq 20]$, kategoriselle vaikkapa $[a = January]$.

\newpage\section{Kuvaukset}
\textit{Kuvaus(description)} koostuu \textit{kyselystä (query)} $q:\mathcal{E}\rightarrow \{true, false\}$ kaikille literaaleille, 
jotka palauttavat totuusarvon jokaiselle entiteetille. Kysely voi olla mikä tahansa totuusarvoinen funktio, 
mutta on kuitenkin hyvä rajoittaa kyseilyitä sellaisiin muotoihin, jotka eivät ole liian monimutkaisia. 
Tätä rajoitusta kutsutaan \textit{kyselykieleksi (query language)} $\mathcal{Q}$, johon jokainen kysely $q$ kuuluu. 
Tällaisia rajoitettuja kyselykieliä ovat \textit{monotoniset konjuktiiviset kyselyt}, 
\textit{lineaarisesti läpikäytävät kyselyt} ja \textit{puun muotoiset kyselyt}. Näiden kyselyiden järjestys ja rakenne 
on sellainen, että ne ovat yksinkertaisia ja myös helpompi prosessoida.

Esimerkkejä kyselykielistä:
\begin{align*}
    &\text{\ \ monotone conjuctive query: } a \land b \lor c &&\\
    &\ \text{\ linearly parsable query: } ((a \lor b) \land \lnot c) \implies a \lor b \land \lnot c &&\\
    &\ \ \text{tree-shaped query: } (a \lor b) \land (a \lor c) \lor (\lnot b \lor \lnot c) &&
\end{align*}
 
\newpage\section{Jälleenkuvaukset}
\textit{Jälleenkuvaus (redescription)} on kuvausten pari $(p, q)$, joille täyttyy ennalta määritelty 
raja \textit{samankaltaisuudelle (similarity)} $\sim$. Tässä kirjallisuuskatsauksessa käytämme samankaltaisuuden laskemiseen 
\textit{Jaccardin etäisyyttä (Jaccard distance)}, jota myös \citet{galbrun18redescription} käyttävät.
Jaccardin etäisyys lasektaan seuraavanlaisesti.
\[
    d(p, q) = 1 - \frac{|supp(p) \cap supp(q)|}{|supp(p) \cup supp(q)|}
\]

Jaccardin etäisyys pohjautuu \textit{Jaccardin indeksiin (Jaccard index)}. 
Merkintä $d(p, q)$ tarkoittaa etäisyyttä kuvausten välillä. Kumpikaan kuvauksien kantajista  
ei saa olla tyhjä. Etäisyysfunktion tarkoituksena on tutkia kuinka samankaltaiset kuvausten 
kantajajoukot ovat. Mikäli kantajajoukot olisivat samat, niin $d(p, q) = 0$, jolloin voidaan 
sanoa jälleenkuvauksen olevan täsmällinen, jota merkitään $p \equiv q$. Jälleenkuvaukset 
kuitenkin ovat harvoin täsmällisiä. Sen sijaan käyttäjän tulee määrittää mikä on riittävän täsmällinen. 
Määritellään siis samankaltaisuudelle raja-arvo $\tau$, jolloin ehto täyttyy 
mikäli $d(p, q) \geq \tau$, jossa $\tau \in [0, \infty)$. Käytämme merkintää $\sim_\tau$, 
kun ehto täyttyy.
\[
    p \sim_\tau q \equiv d(p, q) \geq \tau     
\]
\indent Kuitenkin pelkistämisen vuoksi on kätevämpää käyttää merkintää $p \sim q$. On myös hyvä huomata, 
että $\sim$ ei ole transitiivinen. On siis mahdollista, että $p\sim q$ ja $q\sim r$, 
mutta $p \nsim r$. 
